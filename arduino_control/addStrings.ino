//
// place to set up progmem string constants to save space in static RAM
//

const unsigned char Cmode[]     PROGMEM = "MODE";
const unsigned char Cverify[]   PROGMEM = "VERIFY";
const unsigned char Cdiag[]     PROGMEM = "DIAG";
const unsigned char Ccode[]     PROGMEM = "CODE";
const unsigned char Cload[]     PROGMEM = "LOAD";
const unsigned char CASCII[]    PROGMEM = "ASCII";
const unsigned char CBINARY[]   PROGMEM = "BINARY";
const unsigned char CUSERTAB[]  PROGMEM = "USERTAB";
const unsigned char CON[]       PROGMEM = "ON";
const unsigned char COFF[]      PROGMEM = "OFF";
const unsigned char CBCD[]      PROGMEM = "BCD";
const unsigned char CEBCDIC[]   PROGMEM = "EBCDIC";
const unsigned char CIDLE[]     PROGMEM = "IDLE";
const unsigned char CECHO[]     PROGMEM = "ECHO";
